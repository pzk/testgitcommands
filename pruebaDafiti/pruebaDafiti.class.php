<?php
class pruebaDafiti
{
  public function isStraight(array $cards)
  {
    try {
      if (is_array($cards)) {
        $straigthCheck = 1;
        $sizeHand      = sizeof($cards);
        sort($cards);
        if ($sizeHand >= 5 && $sizeHand <= 7) {
          for ($i = 0; $i <= $sizeHand; $i++) {
            if ($straigthCheck == 5) {
              return true;
            }
            if (isset($cards[$i + 1])) {
              if ($cards[$i + 1] == $cards[$i] + 1) {
                $straigthCheck++;
              } else {
                $straigthCheck = 1;
              }
            } else {
              if ($straigthCheck == 5) {
                return true;
              } else {
                $target = array(2, 3, 4, 5, 14);
                if (count(array_intersect(array_unique($cards), $target)) == count($target)) {
                  return true;
                } else {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (Exception $e) {
      throw new Exception("Error Processing : " . $e->getMessage(), 400);
    }
  }
}
